---
title: "projet_ma"
author: "Fatou Thior"
date: '2023-03-13'
output: html_document
---



```{r}
library(readr)
accidents_corporels_de_la_circulation_2015 <- read_delim("accidents-corporels-de-la-circulation-2015.csv", 
    delim = ";", escape_double = FALSE, trim_ws = TRUE)
View(accidents_corporels_de_la_circulation_2015)
d <- as.data.frame(accidents_corporels_de_la_circulation_2015)
```


```{r}
library(dplyr)
d <- select(d, -c("V1","V2","Sens","PR1","PR"))
```



```{r}
d$`Date et heure` <- as.POSIXct(d$`Date et heure`, format = "%Y-%m-%d %H:%M:%S")
d$`Nombre de voies` <- as.integer(d$`Nombre de voies`)
d$`Identifiant de l'accident`=as.factor(d$`Identifiant de l'accident`)
d$`Conditions atmosphériques`= as.factor(d$`Conditions atmosphériques`)
d$`Code commune` = as.factor(d$`Code commune`)
d$`Code Insee` = as.factor(d$`Code Insee`)
d$`Code Postal` = as.factor(d$`Code Postal`)
d$`Voie réservée` = as.factor(d$`Voie réservée`)
d$`Catégorie route` = as.factor(d$`Catégorie route`)
d$`Année de naissance` =as.factor(d$`Année de naissance`)
d$`Action piéton` = as.factor(d$`Action piéton`)
d$`Existence équipement de sécurité` = as.factor(d$`Existence équipement de sécurité`)
d$`Utilisation équipement de sécurité` = as.factor(d$`Utilisation équipement de sécurité`)
d$`Catégorie d'usager` = as.factor(d$`Catégorie d'usager`)
d$`Motif trajet` = as.factor(d$`Motif trajet`)
d$`Point de choc` = as.factor(d$`Point de choc`)
d$`Obstacle mobile heurté` = as.factor(d$`Obstacle mobile heurté`)
d$`Obstacle fixe heurté` = as.factor(d$`Obstacle fixe heurté`)
d$`Catégorie véhicule`= as.factor(d$`Catégorie véhicule`)
d <- d %>% 
  mutate(
     
     Commune = as.factor(Commune),
     Année = as.factor(Année),
     Mois = as.factor(Mois),
     Jour = as.factor(Jour),
     Lumière = as.factor(Lumière),
     Localisation = as.factor(Localisation),
     Intersection = as.factor(Intersection),
     Collision = as.factor(Collision),
     Département = as.factor(Département),
    Adresse = as.factor(Adresse),
    Latitude = as.integer(Latitude),
    Longitude = as.integer(Longitude),
    Numéro = as.factor(Numéro),
    Coordonnées = as.factor(Coordonnées),
    Surface = as.factor(Surface),
    Circulation = as.factor(Circulation),
    Env1 = as.factor(Env1),
    Plan = as.factor(Plan),
    Profil = as.factor(Profil),
    Infrastructure = as.factor(Infrastructure),
    Situation = as.factor(Situation),
    Sexe = as.factor(Sexe),
    Gravité = as.factor(Gravité),
    Place = as.factor(Place),
    Manœuvre = as.factor(Manœuvre),
    Gps = as.factor(Gps)
  )
str(d)
View(d)
```



```{r}
donnees_accidents <- select(d, c("Commune","Lumière","Localisation",
"Intersection",
"Conditions atmosphériques",
"Collision",
"Surface",
"Circulation",
"Nombre de voies",
"Catégorie route",
"Plan",
"Profil",
"Situation"


))
```



```{r}
str(donnees_accidents)
summary(donnees_accidents)
```

```{r}
colnames(donnees_accidents) = c("Commune","Lumière","Localisation",
"Intersection",
"Conditions_atmosphériques",
"Collision",
"Surface",
"Circulation",
"Nombre_de_voies",
"Catégorie_route",
"Plan",
"Profil",
"Situation")
```


#Analyse bivariée des données accidents:
```{r}
library(mice) # charge la librairie mice
help(mice) # donnee des informations sur mice
dm = mice(donnees_accidents, m = 5, seed=10, print = FALSE, maxit = 50)
summary(dm)
```



```{r}
results <- matrix(NA, ncol(donnees_accidents), ncol(donnees_accidents))
for(i in 1:(ncol(donnees_accidents))) {
  for(j in 1:ncol(donnees_accidents)) {
    results[i,j] <- chisq.test(donnees_accidents[,i], donnees_accidents[,j])$p.value
  }
}
rownames(results) = c("Commune","Lumière","Localisation",
"Intersection",
"Conditions_atmosphériques",
"Collision",
"Surface",
"Circulation",
"Nombre_de_voies",
"Catégorie_route",
"Plan",
"Profil",
"Situation")

colnames(results) = c("Commune","Lumière","Localisation",
"Intersection",
"Conditions_atmosphériques",
"Collision",
"Surface",
"Circulation",
"Nombre_de_voies",
"Catégorie_route",
"Plan",
"Profil",
"Situation")
results

```


```{r}
require("corrplot")

corrplot(results,type="lower", col = 'blue')
```

```{r}
library(plyr)
#install.packages("philentropy")
library(FactoMineR)
library(factoextra)
library(ggplot2)
```

Classification avec variables collision et conditions_atmosphériques
```{r}
mydata = select(donnees_accidents, c("Collision", "Conditions_atmosphériques"))
```


```{r pressure, echo=FALSE}
mydata.mca = MCA(mydata,ncp=10)
mydata.mca$eig
hcpc=HCPC(mydata.mca,nb.clust=-1, proba=1)
```
L'analyse en composantes multiples nous a permis d'obtenir 10 composantes combinaisions linéaires des variables Collision et Conditions_atmosphériquent et qui expliquent 80.46 % de l'information.

```{r}
fviz_cluster(hcpc, geom = "point", ellipse=T)
```
La classification nous a permis avec la méthode Ward nous a permis d'obtenir 10 classes.
```{r}
mydata1<-cbind(mydata,hcpc$call$X[,11][match(rownames(mydata), rownames(hcpc$call$X))])
colnames(mydata1)<-c(colnames(mydata),"Classe")
```

Nombre d'individus dans chaque cluster
```{r}
table(mydata1$Classe)
```
caractéristiques communes entre les individus :
```{r}
hcpc$desc.var
```
On a obtenu les différentes modalités de nos deux variables qui caractérisent chaque cluster.
Le cluster 1 est caractérisé par des accidents survenus avec des collisions multiples (3 véhicules et plus) et une condition atmosphérique normale.

Classification Collision, Lumière
```{r}
mydata = select(donnees_accidents, c("Collision", "Lumière"))
```


```{r pressure, echo=FALSE}
mydata.mca = MCA(mydata,ncp=8)
mydata.mca$eig
hcpc=HCPC(mydata.mca,nb.clust=-1,proba=1)
```

```{r}
fviz_cluster(hcpc, geom = "point", ellipse=T)
```

```{r}
mydata3<-cbind(mydata,hcpc$call$X[,9][match(rownames(mydata), rownames(hcpc$call$X))])
colnames(mydata3)<-c(colnames(mydata),"Classe")
```

Nombre d'individus dans chaque cluster
```{r}
table(mydata3$Classe)
```
caractéristiques communes entre les individus
```{r}
hcpc$desc.var
```

Classification avec variables collision et catégorie_route
```{r}
mydata = select(donnees_accidents, c("Collision", "Catégorie_route"))
```


```{r pressure, echo=FALSE}
mydata.mca = MCA(mydata,ncp=8)
mydata.mca$eig
hcpc=HCPC(mydata.mca,nb.clust=-1,proba=1)
```

```{r}
fviz_cluster(hcpc, geom = "point", ellipse=T)
```

```{r}
mydata2<-cbind(mydata,hcpc$call$X[,9][match(rownames(mydata), rownames(hcpc$call$X))])
colnames(mydata2)<-c(colnames(mydata),"Classe")
```

Nombre d'individus dans chaque cluster
```{r}
table(mydata1$Classe)
```
caractéristiques communes entre les individus
```{r}
hcpc$desc.var
```

Classification avec variables collision , Plan et Surface
```{r}
mydata = select(donnees_accidents, c("Collision", "Plan", "Surface"))
```


```{r pressure, echo=FALSE}
mydata.mca = MCA(mydata,ncp=10)
mydata.mca$eig
hcpc=HCPC(mydata.mca,nb.clust=-1,proba=1)
```

```{r}
fviz_cluster(hcpc, geom = "point", ellipse=T)
```

```{r}
mydata3<-cbind(mydata,hcpc$call$X[,11][match(rownames(mydata), rownames(hcpc$call$X))])
colnames(mydata3)<-c(colnames(mydata),"Classe")
```

Nombre d'individus dans chaque cluster
```{r}
table(mydata3$Classe)
```
caractéristiques communes entre les individus
```{r}
hcpc$desc.var
```




