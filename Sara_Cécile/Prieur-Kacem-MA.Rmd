---
title: "Projet MA"
author: "Cécile Prieur et Sara Kacem"
date: '2023-03-13'
output:
  pdf_document: default
  html_document: default
---
# Visualisation du jeu de données acteurs
```{r}
library(readr)
donnees_acteurs<- read_delim("donnees_conducteurs.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)
View(donnees_acteurs)
```


#Jeu de données

```{r}
library(readr)
data<- read_delim("accidents-corporels-de-la-circulation-2015.csv", 
    ";", escape_backslash = TRUE, escape_double = FALSE, 
    trim_ws = TRUE)
View(data)
```
Variables enlevées:

```{r}
library(dplyr)
d<- select(data,-c("V1", "V2", "PR", "PR1"))
```

```{r}
library(dplyr)
d$`Année de naissance`=as.factor(d$`Année de naissance`)
d$`Date et heure`=as.POSIXct(d$`Date et heure`,format=("%Y-%m-%d %H:%M:%S"))
d$`Code commune`=as.factor(d$`Code commune`)
d$`Code Insee`=as.factor(d$`Code Insee`)
d$`Code Postal`=as.factor(d$`Code Postal`)
d$`Largeur de la chaussée`=as.factor(d$`Largeur de la chaussée`)
d$`Largeur terre plein central`=as.factor(d$`Largeur terre plein central`)
d$`Nombre de voies`=as.factor(d$`Nombre de voies`)
d$`Catégorie route`=as.factor(d$`Catégorie route`)
d$`Existence équipement de sécurité`=as.factor(d$`Existence équipement de sécurité`)
d$`Utilisation équipement de sécurité`=as.factor(d$`Utilisation équipement de sécurité`)
d$`Localisation du piéton`=as.factor(d$`Localisation du piéton`)
d$`Identifiant véhicule`=as.factor(d$`Identifiant véhicule`)
#d$`Catégorie d'usager`=as.factor(d$`Catégorie d'usager`)
d$`Piéton seul ou non`=as.factor(d$`Piéton seul ou non`)
d$`Action piéton`=as.factor(d$`Action piéton`)
d$`Obstacle fixe heurté`=as.factor(d$`Obstacle fixe heurté`)
d$`Nombre d'occupants`=as.factor(d$`Nombre d'occupants`)
d$Longitude=as.integer(d$Longitude)
d$`Identifiant de l'accident`=as.factor(d$`Identifiant de l'accident`)
d$Commune=as.factor(d$Commune)
d$Place=as.factor(d$Place)
d$Mois=as.factor(d$Mois)
d$Jour=as.factor(d$Jour)
d$Département=as.factor(d$Département)
d$Voie=as.factor(d$Voie)
d$Numéro=as.factor(d$Numéro)
d$Surface=as.factor(d$Surface)
d$Circulation=as.factor(d$Circulation)
d$`Voie réservée`=as.factor(d$`Voie réservée`)
d$Env1=as.factor(d$Env1)
d$Plan=as.factor(d$Plan)
d$Profil=as.factor(d$Profil)
d$Infrastructure=as.factor(d$Infrastructure)
d$Situation=as.factor(d$Situation)
d$Sexe=as.factor(d$Sexe)
d$Gravité=as.factor(d$Gravité)
d$`Motif trajet`=as.factor(d$`Motif trajet`)
d$Sens=as.factor(d$Sens)
d$Gps=as.factor(d$Gps)
d$Circulation=as.factor(d$Circulation)
    
    
#Variables à revoir: date de naissance - Point de choc - Sexe - Manoeuvre -  Obstacle mobile heurté - CAtegore véhucule
#Revoir variable heure
```

```{r}
summary(d)
```

```{r}
donnees_acteurs$`Existence.équipement.de.sécurité`=as.factor(donnees_acteurs$`Existence.équipement.de.sécurité`)
donnees_acteurs$`Utilisation.équipement.de.sécurité`=as.factor(donnees_acteurs$`Utilisation.équipement.de.sécurité`)
donnees_acteurs$Sexe=as.factor(donnees_acteurs$Sexe)
donnees_acteurs$Gravité=as.factor(donnees_acteurs$Gravité)

str(donnees_acteurs)
summary(donnees_acteurs)
```
Tableaux de contingence (données conducteur) :
```{r}
print("sexe en fonction de la gravité :")
tab1 <- table(donnees_acteurs$Sexe, donnees_acteurs$Gravité)
tab1

print("sexe en fonction de l'utilisation d'équipement de sécurité:")
tab2 <- table(donnees_acteurs$Sexe, donnees_acteurs$`Utilisation.équipement.de.sécurité`)
tab2

print("utilisation d'équipement de sécurité en fonction de la gravité :")
tab3 <- table(donnees_acteurs$`Utilisation.équipement.de.sécurité`, donnees_acteurs$Gravité)
tab3

print("utilisation d'équipement de sécurité en fonction du motif de trajet :")
tab4 <- table(donnees_acteurs$`Utilisation.équipement.de.sécurité`, donnees_acteurs$`Motif.trajet`)
tab4

print("existence d'équipement de sécurité en fonction d'utilisation d'équipement de sécurité :")
tab5 <- table(donnees_acteurs$`Existence.équipement.de.sécurité` , donnees_acteurs$`Utilisation.équipement.de.sécurité`)
tab5
```

```{r}
print("sexe en fonction de la gravité :")
prop.table(tab1)

print("sexe en fonction de l'utilisation d'équipement de sécurité:")
prop.table(tab2)

print("utilisation d'équipement de sécurité en fonction de la gravité :")
prop.table(tab3)

print("utilisation d'équipement de sécurité en fonction du motif de trajet :")
prop.table(tab4)

print("existence d'équipement de sécurité en fonction d'utilisation d'équipement de sécurité :")
prop.table(tab5)

```

```{r}
mosaicplot(Sexe ~ Gravité, data = donnees_acteurs, shade = TRUE, main = "Graphe en mosaïque")
```
```{r}
#library(vcd)
#mosaic(~ Sexe + Gravité + `Utilisation.équipement.de.sécurité`, donnees_conducteurs, highlighting = "sexe", main = "Exemple de graphique en mosaïque à 3 dimensions")
```


```{r}
library(plyr)
library(dplyr)
library(FactoMineR)
library(factoextra)
library(ggplot2)
```

Réalisation de la classification en utilisant les variables relatives à la lumière lors de l'accident et de la catégorie de véhicule
```{r}
mydata = select(data, c("Lumière", "Identifiant véhicule"))
```

Réalisation de la MCA :
```{r pressure, echo=FALSE}
mydata.mca = MCA(mydata,ncp=10) #choIx de 10 composantes pour la recherche initiale
mydata.mca$eig
hcpc=HCPC(mydata.mca,nb.clust=-1, proba=1)
```

Suite à l'ACM, nous choisissons de garder 22 composantes, (combinaisions linéaires des variables Lumière et Identifiant véhicule).
Ces composantes expliquent au total 80.96% de l'information.

```{r}
fviz_cluster(hcpc, geom = "point", ellipse=T)
```
Nous obtenons ainsi 9 clusters.
```{r}
mydata1<-cbind(mydata,hcpc$call$X[,11][match(rownames(mydata), rownames(hcpc$call$X))])
colnames(mydata1)<-c(colnames(mydata),"Classe")
```


```{r}
table(mydata1$Classe)
```

Ainsi, le cluster 4 est celui comprenant le plus d'individus avec un total de 282 individus, tandis que le cluster 1, 2 , 3 et 5 font penser à des données isolées car ces clusters ne contiennent que de 1 à 5 individus.

Caractérisation pour les différents clusters.
```{r}
hcpc$desc.var
```
Ainsi, par exemple, le cluster 2 est caractérisé par des accidents ayant lieu le jour et impliquant les types de véhicules A01,C01,B01 et D01.