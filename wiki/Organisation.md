# Consignes

## L'unité statistique

Notre unité statistique est **l'accident**.

C'est à dire qu'en règle générale, notre analyse portera sur des statistiques sur les accidents, pas les occupants ou les véhicules.

## Les types d'analyses à réaliser

### Les techniques

Il se trouve qu'il fallait qu'on devine que 

> réaliser plusieurs classifications

qui est notre seule consigne, signifiait en fait qu'il fallait faire des classifications **non supervisées** aka du clustering.

Il faut donc faire des statistiques exploratoires. Vous pouvez faire de l'ACM et de la CAH.

### Les sujets

M. Preda nous a précisé qu'il faut qu'on fasse différentes analyses que l'on trouve intéressantes en se penchant sur des sujets plus précis du dataset. 
Par exemple, on peut essayer d'étudier les accidents incluant des poids lourds, faire différentes analyses pertienentes sur ce sous ensemble des données, et faire du clustering afin de trouver différents types d'accidents avec poids lourds.  

# Tâches par Groupe

## Carla et Loucas

Analyse sur les accidents impliquant des piétons.

## Fatou et Sidy

Description des variables et étude sur les collisions et les conditions extérieures des accidents 

## Arnaud et Axel

Les accidents de nuit. (éclairés et non éclairés)

## Cécile et Sara

Mauvaise météo (ou autre si trop peu de données)
