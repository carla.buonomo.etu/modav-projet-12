# Accueil

Ces pages sont une petite documentation Markdown pour le projet Modélisation Avancée.

On n'est pas très avancés, et le projet prend fin Samedi 15 à 23h59.

Vous pourrez trouver sur la page [[Organisation]] des consignes par rapport à ce qui a été dit par Preda, et une première organisation avec des idées d'analyse exploratoire.

On s'est occupés de réaliser une première [[Analyse Générale]] du dataset à laquelle vous pouvez évidemment contribuer.