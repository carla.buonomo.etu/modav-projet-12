---
title: "analyse_globale"
author: "Carla BUONOMO"
date: '2023-03-31'
output: html_document
---

Données des accidentés de 2015 autour de l'agglomération de Rennes








# Les accidents dans les agglomérations de Rennes


```r
if(!exists("far_map")) 
  far_map <- get_map(location = "rennes, France", zoom = 11, maptype = "terrain", scale = 2)
if(!exists("near_map")) 
  near_map <- get_map(location = "rennes, France", zoom = 12, maptype = "terrain", scale = 2)
```


```r
coordinates <- accidents %>% 
  select(Latitude, Longitude) %>%
  mutate(Latitude = Latitude * 0.00001) %>%
  mutate(Longitude = Longitude * 0.00001) %>%
  na.omit()

ggmap(near_map) +
  geom_point(coordinates, mapping = aes(x = Longitude, y = Latitude), color = "red", size = 1, na.rm = T) +
  scale_size(range = c(1, 12))
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6-1.png)


```r
ggmap(far_map) +
  geom_point(coordinates, mapping = aes(x = Longitude, y = Latitude), color = "red", size = 1, na.rm = T)
```

![plot of chunk unnamed-chunk-7](figure/unnamed-chunk-7-1.png)

Nos données concernent bien Rennes et ses alentours. Il semble y avoir une forte concentration au centre et dans les routes périphériques.

Vérifions combien de coordonnées sont dupliquées.


```r
sum(duplicated(coordinates))
```

```
## [1] 4
```
Seules 4 coordonnées sont dupliquées, ce qui suggère qu'elles ont été entrées la plupart du temps avec une bonne précision, c'est à dire pas simplement sur le centre de la ville.

# Analyse par sexe des conducteurs

Transformation des données


```r
sexe_accidentes <- accidents %>% 
  select(Sexe, Catégorie.d.usager) %>% 
  separate_rows(everything(), sep = ",")

sexe_conducteurs_accidentes <- sexe_accidentes %>% 
  filter(Catégorie.d.usager == "Conducteur") %>% 
  select(Sexe)
```

Barplot des accidents impliquant des conducteurs féminins ou masculins


```r
ggplot(sexe_conducteurs_accidentes, aes(x=Sexe, fill=Sexe)) + geom_bar()
```

![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-10-1.png)


```r
sexe_conducteurs_accidentes %>%
  count(Sexe, name = "count") %>%
  mutate(proportion = count/sum(count)) %>%
  knitr::kable( 
             caption = "Proportions par sexe",
             col.names = c("Sexe", "Nombre", "Proportion"), 
             bold.rownames = TRUE)
```



Table: Proportions par sexe

|Sexe     | Nombre| Proportion|
|:--------|------:|----------:|
|Féminin  |    237|  0.3259972|
|Masculin |    490|  0.6740028|

67,4 % des accidents impliquent des hommes conducteurs, contre seulement 32.6% pour les femmes conductrices.

# Analyse temporelle


```r
moyenne_mobile <- function(x, n = 5){stats::filter(x, rep(1 / n, n), sides = 2)} # Moyenne mobile vers la droite


accidents %>%
  mutate(date = as.Date(Date.et.heure)) %>%
  group_by(date) %>%
  summarize(num_accidents = n()) %>%
  mutate(moyenne_mob = moyenne_mobile(num_accidents, n=7)) %>%
  na.omit() %>%
  ggplot(aes(x = date)) +
  geom_line(aes(y = as.numeric(moyenne_mob)), color = "steelblue") +
  labs(title = "Nombre d'accidents (Moyenne mobile 7-jours)",
       x = "Date",
       y = "Nombre d'accidents") +
  scale_x_date(date_breaks = "1 month", date_labels = "%b %Y") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
```

![plot of chunk unnamed-chunk-12](figure/unnamed-chunk-12-1.png)


```r
accidents %>%
  mutate(month = lubridate::month(Date.et.heure)) %>%
  ggplot(aes(x = factor(month))) +
  geom_bar(fill = "steelblue", width = 0.7) +
  labs(title = "Nombre d'accidents par mois",
       x = "Mois",
       y = "Nombre d'accidents")
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-1.png)

On voit clairement ce qui semble être une saisonalité (bien qu'à prendre avec des pincettes, ayant une seule année de données), avec un premier pic en Juin, une augmentation générale en hiver et particulièrement eb décembre.

# Les accidents par type de véhicule


```r
accidents %>%
  separate_rows(Catégorie.véhicule, sep = ",") %>%
  group_by(Catégorie.véhicule) %>%
  count(name = "n_accidents") %>%
  arrange(desc(n_accidents)) %>%
  ggplot(aes(x = reorder(str_trunc(Catégorie.véhicule, 30), -n_accidents), y = n_accidents)) +
  geom_bar(fill = "steelblue", width = 0.7, stat="identity") +
  labs(x = "Catégorie de véhicule", y = "Nombre d'accidents") +
  coord_flip()
```

![plot of chunk unnamed-chunk-14](figure/unnamed-chunk-14-1.png)


De loin, l'accident le plus commun n'implique qu'un seul véhicule.



Regardons ce graphique en retirant les véhicules seuls.


```r
accidents %>%
  separate_rows(Catégorie.véhicule, sep = ",") %>%
  group_by(Catégorie.véhicule) %>%
  count(name = "n_accidents") %>%
  arrange(desc(n_accidents)) %>%
  filter(Catégorie.véhicule != "VL seul") %>%
  ggplot(aes(x = reorder(str_trunc(Catégorie.véhicule, 30), -n_accidents), y = n_accidents)) +
  geom_bar(fill = "steelblue", width = 0.7, stat="identity") +
  labs(x = "Catégorie de véhicule", y = "Nombre d'accidents") +
  coord_flip()
```

![plot of chunk unnamed-chunk-15](figure/unnamed-chunk-15-1.png)

...

# Conditions météorologiques

...

# Type de route

...























 
