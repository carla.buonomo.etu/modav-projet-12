---
title: "Analyse_pietons"
author: "Carla BUONOMO"
date: '2023-04-14'
output: html_document
---




```r
library(FactoMineR)
library(factoextra)

# install.packages("ggmap")
library(ggmap)

library(tidyverse)
library(gridExtra)
```





```r
accidents_pietons <- accidents %>%
  filter(str_detect(Catégorie.d.usager, "Piéton"))
```


```r
nrow(accidents)
```

```
## [1] 423
```


```r
nrow(accidents_pietons)
```

```
## [1] 70
```

Seuls 70 des 423 accidents concernent des piétons.

# Analyses simples

## Positions des accidents


```r
if(!exists("near_map")) 
  near_map <- get_map(location = "rennes, France", zoom = 12, maptype = "terrain", scale = 2)
```


```r
coordinates <- accidents_pietons %>% 
  select(Latitude, Longitude) %>%
  mutate(Latitude = Latitude * 0.00001) %>%
  mutate(Longitude = Longitude * 0.00001) %>%
  na.omit()

ggmap(near_map) +
  geom_point(coordinates, mapping = aes(x = Longitude, y = Latitude), color = "red", size = 1, na.rm = T) +
  scale_size(range = c(1, 12))
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9-1.png)

# Analyse temporelle



```r
accidents_pietons %>%
  mutate(month = lubridate::month(Date.et.heure)) %>%
  ggplot(aes(x = factor(month))) +
  geom_bar(fill = "steelblue", width = 0.7) +
  labs(title = "Nombre d'accidents par mois",
       x = "Mois",
       y = "Nombre d'accidents")
```

![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-10-1.png)

Les accidents piétons sembent suivre la même saisonnalité que les accidents en général.


## Conducteurs: hommes ou femmes?

Les conducteurs impliqués dans des accidents avec des piétons sont ils plus souvent des femmes ou des hommes?

Accidents piétons:

```r
sexe_conducteurs_accidents_pietons <- accidents_pietons %>% 
  select(Sexe, Catégorie.d.usager) %>% 
  separate_rows(everything(), sep = ",") %>% 
  filter(Catégorie.d.usager == "Conducteur") %>% 
  select(Sexe)
```

Tous accidents:


```r
sexe_conducteurs_accidents <- accidents %>% 
  select(Sexe, Catégorie.d.usager) %>% 
  separate_rows(everything(), sep = ",") %>% 
  filter(Catégorie.d.usager == "Conducteur") %>% 
  select(Sexe)
```




```r
plot_sexe_conducteurs_accidents_pietons <- 
  ggplot(sexe_conducteurs_accidents_pietons, aes(x=Sexe, fill=Sexe)) + ylab("Nombre de conducteurs") + labs(title="Sexe des conducteurs en accidents piétons") + geom_bar()
plot_sexe_conducteurs_accidents <- 
  ggplot(sexe_conducteurs_accidents, aes(x=Sexe, fill=Sexe)) + ylab("Nombre de conducteurs") + labs(title="Sexe des conducteurs en accidents généraux") + geom_bar()

grid.arrange(
  plot_sexe_conducteurs_accidents_pietons,
  plot_sexe_conducteurs_accidents,
  ncol = 2
)
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-1.png)

Les conducteurs hommes font plus d'accidents piétons, mais nous voyons qu'il n'y a pas une grande différence avec les accidents de rennes en général.


## Type de routes

Sur quel genre de routes les accidents piétons ont ils lieux? En commune? En route départementale?


```r
plot_route_accidents_pietons <- accidents_pietons %>%
  select(Catégorie.route) %>%
  ggplot(aes(x=str_trunc(Catégorie.route, 20))) +
  geom_bar(fill="steelblue", width=0.7) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  labs(title = "Routes des accidents piétons") +
  ylab("Nombre d'accidents piétons") +
  xlab("Route")

plot_route_accidents <- accidents %>%
  select(Catégorie.route) %>%
  ggplot(aes(x=str_trunc(Catégorie.route, 20))) +
  geom_bar(fill="steelblue", width=0.7) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  labs(title = "Routes des accidents") +
  ylab("Nombre d'accidents") +
  xlab("Route")

grid.arrange(plot_route_accidents_pietons, plot_route_accidents, ncol=2)
```

![plot of chunk unnamed-chunk-14](figure/unnamed-chunk-14-1.png)

Les accidents piétons arrivent proportionnellement beaucoup moins sur des routes départementales et nationales que les accidents en général à Rennes.

## Conditions météorologiques des accidents piétons

Sous quelles conditions météorologiques les accidents piétons ont ils lieux, et est-ce différent de la plupart des accidents?


```r
plot_meteo_accidents_pietons <- accidents_pietons %>%
  select(Conditions.atmosphériques) %>%
  group_by(Conditions.atmosphériques) %>%
  count(name = "Count") %>%
  arrange(desc(Count)) %>%
  ggplot(aes(x=reorder(Conditions.atmosphériques, -Count), y=Count)) +
  geom_bar(stat="identity", fill="steelblue", width=0.7) +
  ylab("Nombre d'accidents piétons") +
  xlab("Conditions atmosphériques") +
  labs(title="Conditions atmosphériques des accidents piétons") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

plot_meteo_accidents <- accidents %>%
  select(Conditions.atmosphériques) %>%
  group_by(Conditions.atmosphériques) %>%
  count(name = "Count") %>%
  arrange(desc(Count)) %>%
  ggplot(aes(x=reorder(Conditions.atmosphériques, -Count), y=Count)) +
  geom_bar(stat="identity", fill="steelblue", width=0.7) +
  ylab("Nombre d'accidents") +
  xlab("Conditions atmosphériques") +
  labs(title="Conditions atmosphériques des accidents") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

grid.arrange(plot_meteo_accidents_pietons, plot_meteo_accidents, ncol=2)
```

![plot of chunk unnamed-chunk-15](figure/unnamed-chunk-15-1.png)

Il semble que de mauvaises conditions météorologiques soient moins présentes dans les accidents piétons que dans les accidents en général.

## Les conditions lumineuses des accidents piétons


```r
plot_lumiere_accidents_pietons <- accidents_pietons %>%
  select(Lumière) %>%
  group_by(Lumière) %>%
  count(name = "Count") %>%
  arrange(desc(Count)) %>%
  ggplot(aes(x=reorder(Lumière, -Count), y=Count)) +
  geom_bar(stat="identity",fill="steelblue", width=0.7) +
  ylab("Nombre d'accidents piétons") +
  xlab("Lumière") +
  labs(title="Lumière des accidents piétons") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

plot_lumiere_accidents <- accidents %>%
  select(Lumière) %>%
  group_by(Lumière) %>%
  count(name = "Count") %>%
  arrange(desc(Count)) %>%
  ggplot(aes(x=reorder(Lumière, -Count), y=Count)) +
  geom_bar(stat="identity", fill="steelblue", width=0.7) +
  ylab("Nombre d'accidents") +
  xlab("Lumière") +
  labs(title="Lumière des accidents") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

grid.arrange(plot_lumiere_accidents_pietons, plot_lumiere_accidents, ncol=2)
```

![plot of chunk unnamed-chunk-16](figure/unnamed-chunk-16-1.png)

Il est difficile à dire si l'éclairage est différent dans les accidents piétons. Essayons plutot de faire la distinction entre jour et nuit.

## Jour ou nuit dans les accidents piétons


```r
plot_lumiere_accidents_pietons <- accidents_pietons %>%
  select(Lumière) %>%
  mutate(Lumière = if_else(Lumière == "Plein jour", "Jour", "Nuit")) %>%
  group_by(Lumière) %>%
  count(name = "Count") %>%
  arrange(desc(Count)) %>%
  ggplot(aes(x=reorder(Lumière, -Count), y=Count)) +
  geom_bar(stat="identity",fill="steelblue", width=0.7) +
  ylab("Nombre d'accidents piétons") +
  xlab("Lumière") +
  labs(title="Lumière des accidents piétons") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

plot_lumiere_accidents <- accidents %>%
  select(Lumière) %>%
  mutate(Lumière = if_else(Lumière == "Plein jour", "Jour", "Nuit")) %>%
  group_by(Lumière) %>%
  count(name = "Count") %>%
  arrange(desc(Count)) %>%
  ggplot(aes(x=reorder(Lumière, -Count), y=Count)) +
  geom_bar(stat="identity", fill="steelblue", width=0.7) +
  ylab("Nombre d'accidents") +
  xlab("Lumière") +
  labs(title="Lumière des accidents") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

grid.arrange(plot_lumiere_accidents_pietons, plot_lumiere_accidents, ncol=2)
```

![plot of chunk unnamed-chunk-17](figure/unnamed-chunk-17-1.png)

Il n'y a pas de grande différence entre la Nuit et le Jour pour les accidents piétons contre tous les accidents.

# Analyse exploratoire

Nous allons effectuer une ACM sur nos données d'accidents piétons.

## Sous ensemble de variables

Il nous faut d'abord choisir un sous ensemble de données catégoriques à analyser.


```r
accidents_pietons_clean <- accidents_pietons %>%
  select(Lumière, Localisation, Intersection, Conditions.atmosphériques, Catégorie.route, Surface) %>%
  mutate_all(~replace(., . == "", NA))

accidents_pietons_clean
```

Valeurs manquantes:


```r
accidents_pietons_clean %>%
  summarise_all(~sum(is.na(.)))
```

## ACM


```r
accidents_pietons_clean.mca = MCA(accidents_pietons_clean)
```

![plot of chunk unnamed-chunk-20](figure/unnamed-chunk-20-1.png)![plot of chunk unnamed-chunk-20](figure/unnamed-chunk-20-2.png)![plot of chunk unnamed-chunk-20](figure/unnamed-chunk-20-3.png)

```r
fviz_eig(accidents_pietons_clean.mca, ncp = 30)
```

![plot of chunk unnamed-chunk-20](figure/unnamed-chunk-20-4.png)


```r
accidents_pietons_clean.mca$eig 
```

```
##          eigenvalue percentage of variance cumulative percentage of variance
## dim 1  4.259645e-01           1.217041e+01                          12.17041
## dim 2  3.718853e-01           1.062530e+01                          22.79571
## dim 3  3.080299e-01           8.800856e+00                          31.59657
## dim 4  2.576742e-01           7.362119e+00                          38.95868
## dim 5  2.315335e-01           6.615244e+00                          45.57393
## dim 6  2.040544e-01           5.830125e+00                          51.40405
## dim 7  1.739710e-01           4.970599e+00                          56.37465
## dim 8  1.666667e-01           4.761905e+00                          61.13656
## dim 9  1.666667e-01           4.761905e+00                          65.89846
## dim 10 1.666667e-01           4.761905e+00                          70.66037
## dim 11 1.666667e-01           4.761905e+00                          75.42227
## dim 12 1.634489e-01           4.669967e+00                          80.09224
## dim 13 1.385956e-01           3.959874e+00                          84.05211
## dim 14 1.275085e-01           3.643099e+00                          87.69521
## dim 15 1.171764e-01           3.347896e+00                          91.04311
## dim 16 9.104175e-02           2.601193e+00                          93.64430
## dim 17 7.802705e-02           2.229344e+00                          95.87365
## dim 18 5.954556e-02           1.701302e+00                          97.57495
## dim 19 5.478078e-02           1.565165e+00                          99.14011
## dim 20 3.009606e-02           8.598875e-01                         100.00000
## dim 21 2.133065e-31           6.094471e-30                         100.00000
```



```r
accidents_pietons_clean.hcpc = HCPC(accidents_pietons_clean.mca, nb.clust = -1)
```

![plot of chunk unnamed-chunk-22](figure/unnamed-chunk-22-1.png)![plot of chunk unnamed-chunk-22](figure/unnamed-chunk-22-2.png)![plot of chunk unnamed-chunk-22](figure/unnamed-chunk-22-3.png)


```r
fviz_cluster(accidents_pietons_clean.hcpc)
```

![plot of chunk unnamed-chunk-23](figure/unnamed-chunk-23-1.png)


```r
accidents_pietons_clean.hcpc$data.clust %>%
  select(clust) %>%
  group_by(clust) %>%
  count(name="Count") %>%
  ggplot(aes(x = reorder(str_c("Cluster ", clust), -Count), y=Count)) +
  geom_bar(stat="identity") +
  labs(title="Nombre d'individus par cluster") +
  xlab("Cluster") +
  ylab("Nombre d'accidents")
```

![plot of chunk unnamed-chunk-24](figure/unnamed-chunk-24-1.png)

Il faudra donner plus d'attention aux clusters avec plus d'individus



```r
accidents_pietons_clean.hcpc$desc.var
```

```
## 
## Link between the cluster variable and the categorical variables (chi-square test)
## =================================================================================
##                                p.value df
## Lumière                   1.106362e-25 15
## Catégorie.route           3.318370e-20 10
## Surface                   3.965406e-17 10
## Conditions.atmosphériques 4.379020e-11 30
## Localisation              1.517743e-05  5
## 
## Description of each cluster by the categories
## =============================================
## $`1`
##                                         Cla/Mod  Mod/Cla    Global     p.value    v.test
## Surface=Surface.NA                   100.000000 66.66667  2.857143 0.001242236  3.229001
## Catégorie.route=Route Départementale 100.000000 66.66667  2.857143 0.001242236  3.229001
## Intersection=Giratoire                28.571429 66.66667 10.000000 0.025447570  2.234540
## Catégorie.route=Voie Communale         1.492537 33.33333 95.714286 0.003708440 -2.901953
## 
## $`2`
##                                          Cla/Mod Mod/Cla    Global      p.value    v.test
## Intersection=Intersection en X          38.46154     100 18.571429 0.0001063371  3.875658
## Conditions.atmosphériques=Temps couvert 50.00000      40  5.714286 0.0241139934  2.255308
## Intersection=Hors intersection           0.00000       0 50.000000 0.0268224097 -2.214093
## Conditions.atmosphériques=Normale        0.00000       0 71.428571 0.0012810032 -3.220202
## 
## $`3`
##                                             Cla/Mod   Mod/Cla    Global      p.value    v.test
## Surface=normale                           87.234043 95.348837 67.142857 1.530887e-10  6.402265
## Conditions.atmosphériques=Normale         84.000000 97.674419 71.428571 6.006704e-10  6.190255
## Lumière=Plein jour                        77.083333 86.046512 68.571429 1.129202e-04  3.861008
## Conditions.atmosphériques=Temps couvert    0.000000  0.000000  5.714286 1.914069e-02 -2.342780
## Lumière=Nuit avec éclairage public allumé 33.333333 13.953488 25.714286 6.466603e-03 -2.723133
## Conditions.atmosphériques=Pluie légère     8.333333  2.325581 17.142857 5.596420e-05 -4.029212
## Surface=mouillée                           9.523810  4.651163 30.000000 5.400822e-09 -5.834327
## 
## $`4`
##                                            Cla/Mod   Mod/Cla   Global      p.value    v.test
## Surface=mouillée                          71.42857 100.00000 30.00000 7.521199e-11  6.509878
## Conditions.atmosphériques=Pluie légère    91.66667  73.33333 17.14286 7.142190e-09  5.787547
## Lumière=Nuit avec éclairage public allumé 50.00000  60.00000 25.71429 1.712252e-03  3.136075
## Intersection=Intersection en X             0.00000   0.00000 18.57143 3.057321e-02 -2.162584
## Lumière=Plein jour                        12.50000  40.00000 68.57143 1.194301e-02 -2.513824
## Conditions.atmosphériques=Normale          4.00000  13.33333 71.42857 1.370362e-07 -5.269178
## Surface=normale                            0.00000   0.00000 67.14286 6.795941e-10 -6.170767
## 
## $`5`
##                            Cla/Mod Mod/Cla    Global      p.value    v.test
## Lumière=Crépuscule ou aube     100     100  4.285714 1.826818e-05  4.285070
## Lumière=Plein jour               0       0 68.571429 2.813299e-02 -2.195427
## 
## $`6`
##                                      Cla/Mod Mod/Cla    Global    p.value    v.test
## Catégorie.route=Route Nationale    100.00000     100  1.428571 0.01428571  2.449998
## Lumière=Nuit sans éclairage public 100.00000     100  1.428571 0.01428571  2.449998
## Localisation=Hors agglomération     33.33333     100  4.285714 0.04285714  2.025100
## Catégorie.route=Voie Communale       0.00000       0 95.714286 0.04285714 -2.025100
## Localisation=En agglomération        0.00000       0 95.714286 0.04285714 -2.025100
```

Le cluster 3 étant le plus gros, regardons d'abord celui-ci.

On voit que le cluster 3 regroupe a lui seul 87% des accidents piétons sur surface normale, 84% des accidents piétons avec conditions météo normales, et 77% des accidents de plein jour. Ce cluster rassemble donc les accidents piétons qui ont eu lieu dans de bonnes conditions. Étant le plus gros cluster, on en déduit que les accidents piétons ont lieu le plus souvent dans de bonnes conditions de freinage et visibilité.

Pour ce qui est du cluster 4, le deuxième plus gros cluster, il regroupe 71% des accidents piétons à surface mouillée et 91% des accidents piétons avec pluie légère. Une bonne partie des accidents piétons ont donc tout de même lieu avec une légère pluie, ce qui peut ralentir le freinage.

Il y a donc deux catégories majeures d'accidents piétons. La plus commune étant en bonne condition de visibilité et de freinage, et la deuxième catégorie bien moins grande en condition de visibilité moyenne et sous une légère pluie, donc avec surface glissante.














